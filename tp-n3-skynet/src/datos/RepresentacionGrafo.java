package datos;

import java.io.Serializable;

public class RepresentacionGrafo implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	int Extremo_vertice_1, Extremo_vertice_2;
	
	
	
	RepresentacionGrafo(int x,int y){
	
		this.Extremo_vertice_1=x;
		this.Extremo_vertice_2=y;
	}

	public int getX() {

		return Extremo_vertice_1;
	}

	public int getY() {

		return Extremo_vertice_2;
	}
}
