package datos;

import java.awt.Point;
import java.io.BufferedReader;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

public class Json {

	private ArrayList<RepresentacionGrafo> aristas;
	private int cantidad_Vertices;
	private ArrayList<Point> puntos;

	public Json() {
		this.aristas = new ArrayList<RepresentacionGrafo>();
		@SuppressWarnings("unused")
		ArrayList<Point> puntos = new ArrayList<Point>();
	}

	public void crearJSON(String str) {

		Gson gson = new GsonBuilder().setPrettyPrinting().create();
		String json = gson.toJson(this);

		try {
			FileWriter writer = new FileWriter(str);
			writer.write(json);
			writer.close();
		} catch (IOException e) {

			e.printStackTrace();
		}

	}

	public static Json leerArchivoJSON(String archivo) {

		Gson gson = new Gson();
		Json claseJSON = null;

		try {
			BufferedReader br = new BufferedReader(new FileReader(archivo));
			claseJSON = gson.fromJson(br, Json.class);
			return claseJSON;
		} catch (Exception e) {

			throw new IllegalArgumentException("El archivo no existe");
		}
	}

	public ArrayList<RepresentacionGrafo> getAristas() {

		return this.aristas;
	}

	public ArrayList<Point> getPuntos() {

		return this.puntos;
	}

	public int getCantidadVertices() {
		return this.cantidad_Vertices;
	}

	public void setearJSONVertices(int puntoA, int puntoB) {

		this.aristas.add(new RepresentacionGrafo(puntoA, puntoB));

	}

	public void setPuntos(ArrayList<Point> puntos) {

		this.puntos = puntos;
	}

	public void setCantVertices(int cantVert) {

		this.cantidad_Vertices = cantVert;
	}

	public String generarJSON() {

		Gson gson = new GsonBuilder().create();
		String json = gson.toJson(this);

		return json;
	}

}
