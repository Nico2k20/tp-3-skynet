package controlador;

import java.awt.Point;
import java.util.ArrayList;

import logica.Arista;
import logica.Grafo;
import logica.Vertice;
import datos.Json;

public class Controlador {

	private Grafo grafo;

	private Json json;

	public Controlador() {
		this.grafo = new Grafo(0);
		this.json = new Json();
	}

	/////////////////// Metodos grafo///////////////////////////
	public void agregarNVertices(int cantVertices) {
		reiniciarGrafos();
		grafo.agregarNVertices(cantVertices);

	}

	public void agregarArista(int puntoA, int puntoB) {

		this.grafo.agregarArista(puntoA, puntoB);

	}

	protected int getCantidadVertices() {
		return this.grafo.getCantidadDeVertices();

	}

	public String verticesOrdenados() {

		return this.grafo.getVerticesOrdenadosPorGrado().toString();
	}

	public String verticesOrdenadosNoVecinos() {

		return this.grafo.getVerticesNoVecinos().toString();
	}

	public String listaVecinos() {

		return this.grafo.listasVecinos();
	}

	public String toString() {

		return this.grafo.toString();

	}

	/////////////// Dominantes Fuerza bruta//////////////////////

	public ArrayList<Integer> generarConjDomFuerzaBruta() {
		return this.grafo.conjDomMinimoFuerzaBruta();

	}

	public String estadisticasFuerzaBruta() {

		return this.grafo.estadisticasFuerzaBruta();
	}

	///////////// Dominantes Heuristica/////////////7

	public String verticesDominantes() {

		return this.grafo.verticesDominantes();

	}

	public String StringconjDominanteMasChico() {

		return this.grafo.getMejorCONJVerticesDominantes().toString();
	}

	public ArrayList<Integer> arrayListPostVerticesDom() {

		ArrayList<Vertice> vertices = this.grafo.getMejorCONJVerticesDominantes();
		ArrayList<Integer> ids = new ArrayList<Integer>();
		for (Vertice v : vertices) {
			ids.add(v.getId());

		}
		return ids;
	}

	public ArrayList<Integer> arrayListPosVerticesDomRandom() {

		ArrayList<Vertice> vertices = this.grafo.getRandomConjVerticesDominantes();
		ArrayList<Integer> ids = new ArrayList<Integer>();
		for (Vertice v : vertices) {
			ids.add(v.getId());

		}
		return ids;

	}

	//////////////// CREAR REPRESENTACION JSON//////////////////

	public void crearJSON(String nombre, ArrayList<Point> puntos) {
		this.json=new Json();
		this.json.setCantVertices(this.grafo.getCantidadDeVertices());

		ArrayList<Arista> copiaAristas = this.grafo.copiaAristas();

		for (int i = 0; i < this.grafo.cantAristas(); i++) {

			agregarAristaJSON(copiaAristas, i);
		}

		agregarCoordenada(puntos, json);

		
		this.json.crearJSON(nombre);

	}

	private void agregarCoordenada(ArrayList<Point> puntos, Json json) {
		json.setPuntos(puntos);
	}

	private void agregarAristaJSON(ArrayList<Arista> copiaAristas, int i) {
		this.json.setearJSONVertices(copiaAristas.get(i).getExtremo1().getId(),
				copiaAristas.get(i).getExtremo2().getId());
	}

	public void levantarJSON(String nombre) {

		// vaciamos el grafo
		this.grafo.vaciarGrafo();

		Json archivo = Json.leerArchivoJSON(nombre);

		// Asignamos el json a la instancia de la clase para poder utilizarlo
		this.json = archivo;

		int cantVertices = archivo.getCantidadVertices();

		this.grafo.agregarNVertices(cantVertices);

		for (int i = 0; i < archivo.getAristas().size(); i++)
			this.grafo.levantarVerticesYaristas(archivo.getAristas().get(i).getX(), archivo.getAristas().get(i).getY());

	}

	public ArrayList<Point> coordenadas() {

		return this.json.getPuntos();
	}

	public ArrayList<ArrayList<Integer>> darVecinos() {

		ArrayList<ArrayList<Integer>> vecinos = new ArrayList<ArrayList<Integer>>();

		for (int i = 0; i < this.json.getAristas().size(); i++) {
			ArrayList<Integer> sonVecinos = new ArrayList<Integer>();

			// agregamos el punto A
			sonVecinos.add(this.json.getAristas().get(i).getX());
			// Agregamos el punto B
			sonVecinos.add(this.json.getAristas().get(i).getY());
			// Agregamos la lista con los vecinos
			vecinos.add(sonVecinos);
		}

		return vecinos;
	}

	//////////////// Reiniciar Grafos//////////////////////////
	public void reiniciarGrafos() {
		this.grafo = new Grafo(0);

	}

	public void reiniciarAristas() {
		int tamGrafo = this.grafo.getCantidadDeVertices();
		this.grafo = new Grafo(tamGrafo);

	}

}
