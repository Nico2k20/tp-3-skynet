package logica;

import static org.junit.Assert.*;

import org.junit.Test;

public class _AristaTest {

	@Test
	public void testAristaValida() {
		Vertice v = new Vertice(0);
		Vertice w = new Vertice(1);
		Arista a = new Arista(v, w);
		boolean resultado = a.getExtremo1().compareTo(v) == 0 && a.getExtremo2().compareTo(w) == 0;
		assertTrue(resultado);
	}

	@Test(expected = NullPointerException.class)
	public void testAristaInvalidaPorVerticeNull() {
		Vertice v = new Vertice(0);
		Vertice w = null;
		@SuppressWarnings("unused")
		Arista a = new Arista(v, w);

	}

	@Test(expected = IllegalArgumentException.class)
	public void testAristaInvalidaPorMismoVerticeEnExtremos() {
		Vertice v = new Vertice(0);
		@SuppressWarnings("unused")
		Arista a = new Arista(v, v);
	}


	@Test
	public void testSonIguales() {
		Vertice v = new Vertice(0);
		Vertice w = new Vertice(1);
		Arista a = new Arista(v, w);

		Arista b = new Arista(v, w);

		assertEquals(0, a.compareTo(b));
	}

	@Test
	public void testSonDistintasPorUnVertice() {
		Vertice v = new Vertice(0);
		Vertice w = new Vertice(1);
		Vertice z = new Vertice(2);

		Arista a = new Arista(v, w);
		Arista b = new Arista(v, z);

		assertNotEquals(0, a.compareTo(b));
	}


}
