package logica;

import static org.junit.Assert.*;

import java.util.ArrayList;

import org.junit.Test;

public class _HeuristicaTest {

////////////////////////Test algoritmo heuristico/////////////////////////////////////7
	@Test
	public void testalgoritmoHeuristico() {
		Grafo grafo = new Grafo(5);

		verticeAisladoYUnArbolTam4(grafo);

		ArrayList<Vertice> conjDom = grafo.getMejorCONJVerticesDominantes();

		int[] retorno = new int[2];

		for (int i = 0; i < conjDom.size(); i++) {
			retorno[i] = conjDom.get(i).getId();

		}

		assertArrayEquals(new int[] { 4, 0 }, retorno);

	}

	@Test
	public void testDameArrayOrdenado() {
		Grafo grafo = new Grafo(5);

		this.verticeAisladoYUnArbolTam4(grafo);

		ArrayList<Vertice> array = Heuristica.dameArrayOrdenado(grafo.getCopiaVertices());

		int verticeMax = array.get(0).getGrado();

		boolean estaOrdenado = true;

		for (int i = 0; i < array.size(); i++) {

			if (verticeMax < array.get(i).getGrado()) {

				estaOrdenado = false;
				assertTrue(estaOrdenado);
			}

		}

		assertTrue(estaOrdenado);

	}

	@Test
	public void testConjVerticesNoVecinos() {
		Grafo grafo = new Grafo(5);

		verticeAisladoYUnArbolTam4(grafo);

		ArrayList<ArrayList<Vertice>> vertices = Heuristica.conjVerticesNoVecinos(grafo.getCopiaVertices());

		ArrayList<Vertice> fila;

		for (int i = 0; i < vertices.size(); i++) {
			fila = vertices.get(i);
			for (int j = 0; j < fila.size(); j++) {

				for (int z = 0; z < fila.size(); z++) {

					if (vertices.get(i).get(j).esVecinoDe(fila.get(z)))
						assertTrue(false);

				}

			}

		}
		assertTrue(true);
	}

	@Test
	public void testConjVerticesNoVecinosConV() {
		Grafo grafo = new Grafo(5);

		verticeAisladoYUnArbolTam4(grafo);

		ArrayList<Vertice> supuestosVerticesNoVecinosAV = Heuristica.conjVerticesNoVecinosConV(grafo.getCopiaVertices(),
				grafo.getVerticePorId(0));

		int[] verticesNoVecinosTest = new int[] { 1, 2, 3, 4 };

		boolean esta = false;

		for (int i : verticesNoVecinosTest) {

			for (Vertice v : supuestosVerticesNoVecinosAV) {
				if (i == v.getId())
					esta = true;

			}

			if (esta == false)
				assertTrue(false);

		}

		assertTrue(true);

	}

	@Test
	public void testBuscarVerticeUnicoDominante() {
		Grafo grafito = new Grafo(3);

		armarK3(grafito);

		Vertice v = grafito.getMejorCONJVerticesDominantes().get(0);

		assertTrue(v.getGrado() == 0 || v.getGrado() == 1 || v.getGrado() == 2);
	}

	//////////////// Metodos vertices///////////////
	private void verticeAisladoYUnArbolTam4(Grafo grafo) {

		grafo.agregarArista(1, 4);
		grafo.agregarArista(2, 4);
		grafo.agregarArista(3, 4);
	}

	private void armarK3(Grafo grafo) {
		grafo.agregarArista(0, 1);
		grafo.agregarArista(0, 2);
		grafo.agregarArista(1, 2);
	}

}
