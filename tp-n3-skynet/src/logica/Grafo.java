package logica;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Random;

public class Grafo {

	private List<Integer> raices;
	private List<Vertice> vertices;
	private ArrayList<Arista> aristas;

	// Constructor
	public Grafo(int n) {
		if (n < 0) {
			throw new IllegalArgumentException("El grafo no puede tener una cantidad negativa de vertices");
		}
		this.raices = new ArrayList<Integer>();
		this.vertices = new ArrayList<Vertice>();
		this.aristas = new ArrayList<Arista>();

		for (int i = 0; i < n; i++) {
			Vertice v = new Vertice(i);
			this.vertices.add(v);

		}

	}

	// Metodos propios de la clase

	public void agregarArista(int i, int j) {
		if (i < 0 || j < 0)
			throw new IllegalArgumentException("Los vertices no pueden ser negativos.");

		if (!this.getIdsVertices().contains(i) || !this.getIdsVertices().contains(j))
			throw new IllegalArgumentException("Los vertices (" + i + "," + j + ") deben estar en el grafo.");

		if (this.existeAristaEntre(i, j)) {
			return;
		}
		// Hago vecinos a ambos vertices entre si
		this.getVerticePorId(i).agregarVecino(this.getVerticePorId(j));
		this.getVerticePorId(j).agregarVecino(this.getVerticePorId(i));

		Arista a = new Arista(this.getVerticePorId(i), this.getVerticePorId(j));// Guardo la arista con su peso
		this.aristas.add(a);
		BFS.actualizarPadresPorAristaModificada(this, i, j);
	}

	// devuelve un string con las listas de vecinos de cada vertice
	public String listasVecinos() {
		int indice = 0;
		String string = "{ \n";
		while (indice < this.vertices.size()) {

			string += "Vertice :  " + this.vertices.get(indice).getId() + " " + this.vertices.get(indice).getVecinos()
					+ "\n";

			indice++;
		}

		string += " }";

		return string;
	}

	// devuelve si el grafo tiene todos sus vertices aislados

	public boolean verticesAislados() {

		for (Vertice v : this.vertices) {

			if (v.tieneVecinos())
				return false;
		}

		return true;
	}

	///////////// Devuelve si el grafo es completo////////////77

	public boolean grafoCompleto() {

		for (Vertice v : vertices) {
			if (v.getGrado() != this.getCantidadDeVertices() - 1)
				return false;

		}
		return true;
	}

	public boolean existeAristaEntre(int i, int j) {

		if (i < 0 || j < 0) {
			throw new IllegalArgumentException("Los vertices no pueden ser negativos.");
		}
		if (!this.getIdsVertices().contains(i) || !this.getIdsVertices().contains(j))
			throw new IllegalArgumentException("Los vertices (" + i + "," + j + ") deben estar en el grafo.");
		// Chequeo que los vertices ingresados sean vecinos entre si.
		if (this.vertices.get(i).esVecinoDe(this.vertices.get(j))
				|| this.vertices.get(j).esVecinoDe(this.vertices.get(i)))
			return true;
		return false;
	}

	// Getters y Setters

	public ArrayList<Arista> copiaAristas() {
		ArrayList<Arista> copiaAristas = new ArrayList<Arista>();

		for (Arista arista : this.aristas) {
			copiaAristas.add(arista);
		}

		return copiaAristas;

	}

	protected List<Integer> getRaices() {
		return raices;
	}

	protected void setRaices(ArrayList<Integer> raices) {
		this.raices = raices;
	}

	protected List<Vertice> getVertices() {
		return vertices;
	}

	protected Vertice getVerticePorId(int id) {
		for (int i = 0; i < this.vertices.size(); i++) {
			if (this.vertices.get(i).getId() == id)
				return this.vertices.get(i);
		}
		return null;
	}

	protected int getIndiceDeVerticeConId(int id) {
		for (int i = 0; i < this.vertices.size(); i++) {
			if (this.getVertices().get(i).getId() == id) {
				return i;
			}
		}
		return -1;
	}

	public int getCantidadDeVertices() {
		return this.vertices.size();
	}

	protected List<Integer> getIdsVertices() {
		ArrayList<Integer> listaIds = new ArrayList<Integer>();
		for (Vertice v : this.vertices) {
			listaIds.add(v.getId());
		}
		return listaIds;
	}

	public void agregarVertice(Vertice v) {
		this.vertices.add(v);
	}

	public void agregarNVertices(int n) {
		if (n < 0) {
			throw new IllegalArgumentException("El grafo no puede tener una cantidad negativa de vertices");
		}

		for (int i = 0; i < n; i++) {
			Vertice v = new Vertice(i);
			agregarVertice(v);
		}
	}

	protected boolean contieneAlVertice(Vertice v) {
		for (Vertice vertice : this.vertices) {
			if (vertice.compareTo(v) == 0)
				return true;
		}
		return false;
	}

	public List<Arista> getAristas() {
		return aristas;
	}

	public int cantAristas() {
		return this.aristas.size();

	}

	protected ArrayList<Vertice> getCopiaVertices() {
		ArrayList<Vertice> copiaVertices = new ArrayList<Vertice>();

		for (Vertice v : this.vertices) {
			copiaVertices.add(v);
		}

		return copiaVertices;
	}

	public int getIDExtremo1Arista(int i) {

		return this.aristas.get(i).getExtremo1().getId();
	}

	public int getIDExtremo2Arista(int i) {

		return this.aristas.get(i).getExtremo2().getId();
	}

	// Metodo Override
	@Override
	public String toString() {
		return "Grafo {raices =" + raices + ", \n vertices=" + vertices + ", \n aristas=" + aristas + "}\n";
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((aristas == null) ? 0 : aristas.hashCode());
		result = prime * result + ((vertices == null) ? 0 : vertices.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == null && obj != null || this != null && obj == null)
			return false;
		Grafo otro = (Grafo) obj;
		if (this.getCantidadDeVertices() != otro.getCantidadDeVertices()
				|| this.getAristas().size() != otro.getAristas().size())
			return false;
		if (!this.getVertices().containsAll(otro.getVertices()) || !this.getAristas().containsAll(otro.getAristas()))
			return false;
		return true;
	}

	public ArrayList<Vertice> getVerticesOrdenadosPorGrado() {
		return Heuristica.dameArrayOrdenado((ArrayList<Vertice>) this.vertices);
	}

	public ArrayList<ArrayList<Vertice>> getVerticesNoVecinos() {

		return Heuristica.conjVerticesNoVecinos(getCopiaVertices());
	}

	public String verticesDominantes() {

		return Heuristica.algoritmoHeuristico(this).toString();

	}

	public ArrayList<ArrayList<Vertice>> getVerticesDominantes() {

		return Heuristica.algoritmoHeuristico(this);

	}

	public ArrayList<Vertice> getMejorCONJVerticesDominantes() {

		// Da un conjunto de conjuntos de vertices dominantes
		ArrayList<ArrayList<Vertice>> conjuntoVerticesDominantes = Heuristica.algoritmoHeuristico(this);

		ArrayList<Vertice> conjMinimoDom = conjuntoVerticesDominantes.get(0);

		for (int i = 0; i < conjuntoVerticesDominantes.size(); i++) {

			if (conjuntoVerticesDominantes.get(i).size() < conjMinimoDom.size()) {
				conjMinimoDom = conjuntoVerticesDominantes.get(i);

			}

		}

		return conjMinimoDom;

	}

	// Obtiene un conjunto random de vertices////////////////
	public ArrayList<Vertice> getRandomConjVerticesDominantes() {

		// Da un conjunto de conjuntos de vertices dominantes
		ArrayList<ArrayList<Vertice>> conjVerticesDominantes = Heuristica.algoritmoHeuristico(this);

		// Da un conj random de vertices
		Random ran = new Random();
		int r = (ran.nextInt(conjVerticesDominantes.size()));

		ArrayList<Vertice> conjMinimoDom = conjVerticesDominantes.get(r);

		return conjMinimoDom;
	}

	/////////// -----Algoritmos FUERZA BRUTA--------/////////////

	public ArrayList<Integer> conjDomMinimoFuerzaBruta() {

		HashSet<Integer> set = FuerzaBruta.conjuntoDominanteMinimoAlAzar(this);

		ArrayList<Integer> retorno = new ArrayList<Integer>();

		for (Integer integer : set) {
			retorno.add(integer);
		}

		return retorno;

	}

	public String estadisticasFuerzaBruta() {
		String retorno = "Conjuntos dominantes minimos:\n" + FuerzaBruta.conjuntosDominantesMinimos(this)
				+ "\nConjuntos dominantes:\n" + FuerzaBruta.conjuntosDominantes(this);

		return retorno;
	}

	public void levantarVerticesYaristas(int puntoA, int puntoB) {

		// agrego las aristas
		this.agregarArista(puntoA, puntoB);

	}

	public void vaciarGrafo() {
		// vacio el grafo actual
		this.vertices = new ArrayList<Vertice>();
		this.aristas = new ArrayList<Arista>();
		this.raices = new ArrayList<Integer>();
	}

}
