package logica;

import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Random;

import org.junit.Test;

public class _FuerzaBrutaTest {

	// Tests del metodo verticesDominables()

	@Test(expected = IllegalArgumentException.class)
	public void dominablesPorConjuntoInvalidoTest() {
		Grafo g = new Grafo(5);
		int[] conjunto = { 5 };
		@SuppressWarnings("unused")
		HashSet<Integer> dominables = FuerzaBruta.verticesDominablesPorElConjunto(g, toHashSet(conjunto));
	}

	@Test
	public void dominablesPorElConjuntoGrafoVacioTest() {
		Grafo g = new Grafo(0);
		HashSet<Integer> conjuntoDeVertices = new HashSet<Integer>();
		conjuntoDeVertices.add(0);
		HashSet<Integer> verticesDominables = FuerzaBruta.verticesDominablesPorElConjunto(g, conjuntoDeVertices);
		assertEquals(0, verticesDominables.size());
	}

	@Test
	public void dominablesPorConjuntoVacioGrafoVerticeAisladoTest() {
		Grafo g = new Grafo(1);
		HashSet<Integer> verticesDominables = FuerzaBruta.verticesDominablesPorElConjunto(g, new HashSet<Integer>());
		assertEquals(1, verticesDominables.size());
	}

	@Test
	public void dominablesPorConjuntoConTodosLosVerticesDelGrafoTest() {
		Grafo g = generarGrafoSinAristasAleatorio();
		HashSet<Integer> conjuntoDeVertices = new HashSet<Integer>();
		conjuntoDeVertices.addAll(g.getIdsVertices());
		assertTrue(FuerzaBruta.verticesDominablesPorElConjunto(g, conjuntoDeVertices).isEmpty());

	}

	@Test
	public void dominablesPorConjuntoParticularTest() {
		Grafo g = new Grafo(10);

		int[] conjunto = { 0, 3, 5, 7 };
		HashSet<Integer> conjuntoDominable = FuerzaBruta.verticesDominablesPorElConjunto(g, toHashSet(conjunto));
		int[] esperado = { 1, 2, 4, 6, 8, 9 };

		assertTrue(conjuntoDominable.size() == 6 && conjuntoDominable.containsAll(toHashSet(esperado)));

	}

	// Tests del metodo verticesVecinosAlConjunto()

	@Test(expected = IllegalArgumentException.class)
	public void vecinosDeConjuntoInvalidoTest() {
		Grafo g = new Grafo(5);

		int[] conjunto = { 5 };
		@SuppressWarnings("unused")
		HashSet<Integer> dominables = FuerzaBruta.verticesVecinosAlConjunto(g, toHashSet(conjunto));
	}

	@Test
	public void vecinosDeConjuntoVacioTest() {
		Grafo g = new Grafo(1);

		assertTrue(FuerzaBruta.verticesVecinosAlConjunto(g, new HashSet<Integer>()).isEmpty());

	}

	@Test
	public void vecinosEnGrafoVacioTest() {
		Grafo g = new Grafo(0);
		assertTrue(FuerzaBruta.verticesVecinosAlConjunto(g, new HashSet<Integer>()).isEmpty());

	}

	public void vecinosEnGrafoDeVerticesAisladosTest() {
		Grafo g = generarGrafoSinAristasAleatorio();
		HashSet<Integer> conjunto = new HashSet<Integer>();
		for (Integer i : g.getIdsVertices()) {
			conjunto.add(i);
		}
		assertEquals(g.getCantidadDeVertices(), FuerzaBruta.verticesVecinosAlConjunto(g, conjunto));
	}

	@Test
	public void vecinosGrafoConexoTest() {
		Grafo g = inicializarGrafoConexo();
		int[] conjunto = { 0, 5 };
		assertEquals(5, FuerzaBruta.verticesVecinosAlConjunto(g, toHashSet(conjunto)).size());
	}

	@Test
	public void vecinosTodosGrafoDisconexoTest() {
		Grafo g = inicializarGrafoDisconexo();
		int[] conjunto = { 0, 3, 6 };
		assertEquals(6, FuerzaBruta.verticesVecinosAlConjunto(g, toHashSet(conjunto)).size());
	}

	@Test
	public void vecinosAlgunosGrafoDisconexoTest() {
		Grafo g = inicializarGrafoDisconexo();
		int[] conjunto = { 0, 3 };
		assertEquals(4, FuerzaBruta.verticesVecinosAlConjunto(g, toHashSet(conjunto)).size());
	}

	// Tests del metodo conjuntosDominantes

	@Test
	public void conjuntosDominantesDeGrafoVacioTest() {
		Grafo g = new Grafo(0);
		ArrayList<HashSet<Integer>> conjuntosDominantes = FuerzaBruta.conjuntosDominantes(g);
		assertTrue(conjuntosDominantes.size() == 0);

	}

	@Test
	public void conjuntosDominantesDeGrafoConVerticesAisladosTest() {
		Grafo g = generarGrafoSinAristasAleatorio();
		ArrayList<HashSet<Integer>> conjuntosDominantes = FuerzaBruta.conjuntosDominantes(g);
		HashSet<Integer> esperado = new HashSet<Integer>();
		for (Integer id : g.getIdsVertices()) {
			esperado.add(id);
		}
		assertTrue(conjuntosDominantes.size() == 1 && conjuntosDominantes.contains(esperado));
	}

	@Test
	public void conjuntosDominantesDeGrafoConexoTest() {
		Grafo g = new Grafo(2);
		g.agregarArista(0, 1);
		ArrayList<HashSet<Integer>> conjuntosDominantes = FuerzaBruta.conjuntosDominantes(g);

		int tamanoConjuntoDominanteMinimo = conjuntosDominantes.get(0).size();
		assertEquals(1, tamanoConjuntoDominanteMinimo);

	}

	@Test
	public void conjuntosDominantesDeGrafoDisconexoTest() {
		Grafo g = inicializarGrafoDisconexo();
		ArrayList<HashSet<Integer>> conjuntosDominantes = FuerzaBruta.conjuntosDominantes(g);
		int[] regionesDelGrafo = { 2, 5, 8 };
		boolean hayUnVerticeDeCadaRegionEnTodosLosConjuntos = true;

		for (HashSet<Integer> conjDom : conjuntosDominantes) {
			HashSet<Integer> regionesDelConjunto = new HashSet<Integer>();
			for (Integer n : conjDom) {
				regionesDelConjunto.add(g.getVerticePorId(n).getPadre());
			}

			if (!(regionesDelConjunto.size() == 3 || regionesDelConjunto.containsAll(toHashSet(regionesDelGrafo))))
				hayUnVerticeDeCadaRegionEnTodosLosConjuntos = false;
		}
		assertTrue(hayUnVerticeDeCadaRegionEnTodosLosConjuntos);

	}

	// Tests del metodo conjuntoDominanteMinimoAlAzar

	@Test
	public void conjuntoDominanteMinimoDeGrafoVacioTest() {
		Grafo g = new Grafo(0);
		HashSet<Integer> conjuntoDominanteMinimo = FuerzaBruta.conjuntoDominanteMinimoAlAzar(g);
		assertTrue(conjuntoDominanteMinimo.isEmpty());
	}

	@Test
	public void conjuntoDominanteMinimoDeGrafoConVerticesAisladosTest() {
		Grafo g = generarGrafoSinAristasAleatorio();
		HashSet<Integer> conjuntoDominanteMinimo = FuerzaBruta.conjuntoDominanteMinimoAlAzar(g);
		HashSet<Integer> esperado = new HashSet<Integer>();
		for (int n : g.getIdsVertices()) {
			esperado.add(n);
		}
		assertTrue(conjuntoDominanteMinimo.size() == g.getCantidadDeVertices()
				&& conjuntoDominanteMinimo.containsAll(esperado));
	}

	@Test
	public void conjuntoDominanteMinimoDeGrafoConexoTest() {
		Grafo g = inicializarGrafoConexo();
		HashSet<Integer> conjuntoDominanteMinimo = FuerzaBruta.conjuntoDominanteMinimoAlAzar(g);
		assertEquals(3, conjuntoDominanteMinimo.size());
	}

	@Test
	public void conjuntoDominanteMinimoDeGrafoDisconexoTest() {
		Grafo g = inicializarGrafoDisconexo();
		HashSet<Integer> conjuntoDominanteMinimo = FuerzaBruta.conjuntoDominanteMinimoAlAzar(g);
		HashSet<Integer> regionesDeLosVerticesDelConjuntoDominanteMinimo = new HashSet<Integer>();
		for (Integer n : conjuntoDominanteMinimo) {
			regionesDeLosVerticesDelConjuntoDominanteMinimo.add(n);
		}
		assertTrue(regionesDeLosVerticesDelConjuntoDominanteMinimo.size() == 3);
	}

	// Metodos auxiliares

	private static Grafo generarGrafoSinAristasAleatorio() {
		Random random = new Random();
		int tamanoDeGrafo = random.nextInt(11);
		return new Grafo(tamanoDeGrafo);

	}

	private static Grafo inicializarGrafoConexo() {
		Grafo g = new Grafo(9);

		g.agregarArista(0, 1);
		g.agregarArista(0, 2);
		g.agregarArista(1, 2);

		g.agregarArista(1, 7);

		g.agregarArista(2, 3);

		g.agregarArista(3, 4);
		g.agregarArista(3, 5);
		g.agregarArista(4, 5);

		g.agregarArista(5, 6);

		g.agregarArista(6, 7);
		g.agregarArista(6, 8);
		g.agregarArista(7, 8);

		return g;
	}

	private static Grafo inicializarGrafoDisconexo() {
		Grafo g = new Grafo(9);

		g.agregarArista(0, 1);
		g.agregarArista(0, 2);
		g.agregarArista(1, 2);

		g.agregarArista(3, 4);
		g.agregarArista(3, 5);
		g.agregarArista(4, 5);

		g.agregarArista(6, 7);
		g.agregarArista(6, 8);
		g.agregarArista(7, 8);

		return g;
	}

	private static HashSet<Integer> toHashSet(int[] conjunto) {
		HashSet<Integer> parse = new HashSet<Integer>();
		for (Integer i : conjunto) {
			parse.add(i);
		}
		return parse;
	}

}
