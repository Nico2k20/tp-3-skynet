package logica;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashSet;
import java.util.List;
import java.util.Random;
import java.util.Set;

public class FuerzaBruta {

	// Variables auxiliares para el algoritmo de fuerza bruta
	private static HashSet<Integer> actual;
	private static ArrayList<HashSet<Integer>> conjuntosDominantesActuales;

	// devuelve un conjunto dominante de tamano minimo entre los hallados
	protected static HashSet<Integer> conjuntoDominanteMinimoAlAzar(Grafo g) {

		ArrayList<HashSet<Integer>> conjuntosDominantesMinimos = conjuntosDominantesMinimos(g);
		if (conjuntosDominantesMinimos.size() == 0)
			return new HashSet<Integer>();
		Random random = new Random();
		// retorno un conjunto de tamano minimo al azar
		return conjuntosDominantesMinimos.get(random.nextInt(conjuntosDominantesMinimos.size()));
	}

	// devuelve todos los conjuntos dominantes con el menor tamano hallados
	protected static ArrayList<HashSet<Integer>> conjuntosDominantesMinimos(Grafo g) {
		ArrayList<HashSet<Integer>> conjuntosDominantes = conjuntosDominantes(g);
		ArrayList<HashSet<Integer>> conjuntosDominantesMinimos = new ArrayList<HashSet<Integer>>();
		if (conjuntosDominantes.size() != 0) {
			int tamanoMinimo = conjuntosDominantes.get(0).size();
			for (HashSet<Integer> conjunto : conjuntosDominantes) {
				if (conjunto.size() == tamanoMinimo) {
					conjuntosDominantesMinimos.add(conjunto);
				}
			}
		}
		return conjuntosDominantesMinimos;
	}

	// devuelve todos los conjuntos dominantes hallados
	protected static ArrayList<HashSet<Integer>> conjuntosDominantes(Grafo g) {
		if (g.getVertices().size() == 0)
			return new ArrayList<HashSet<Integer>>();
		if (g.grafoCompleto()) {
			ArrayList<HashSet<Integer>> ret = new ArrayList<HashSet<Integer>>();
			for (Integer vertice : g.getIdsVertices()) {
				HashSet<Integer> conjunto = new HashSet<Integer>();
				conjunto.add(vertice);
				ret.add(conjunto);
			}
			return ret;

		}
		if (g.getAristas().size() == 0) {
			ArrayList<HashSet<Integer>> ret = new ArrayList<HashSet<Integer>>();
			ret.add(toHashSetInteger(g.getIdsVertices()));
			return ret;

		}
		conjuntosDominantesActuales = new ArrayList<HashSet<Integer>>();
		actual = new HashSet<Integer>();
		generarDesde(g, 0);

		ordenarDeMenorAMayorPorTamaño(conjuntosDominantesActuales);
		return conjuntosDominantesActuales;

	}

	// metodo recursivo auxiliar de conjuntosDominantes
	private static void generarDesde(Grafo g, int vertice) {
		// Caso Base
		if (vertice == g.getVertices().size()) {
			return;
		} else {
			// Llamada recursiva

			if (esDominante(g, actual) && !conjuntosDominantesActuales.contains(getActual())) {
				conjuntosDominantesActuales.add(getActual());

			}

			actual.add(vertice);
			generarDesde(g, vertice + 1);
			actual.remove(vertice);
			generarDesde(g, vertice + 1);
		}

	}

	private static void ordenarDeMenorAMayorPorTamaño(ArrayList<HashSet<Integer>> conjuntosDominantes) {

		Collections.sort(conjuntosDominantes, new Comparator<Set<Integer>>() {
			@Override
			public int compare(Set<Integer> o1, Set<Integer> o2) {
				return Integer.valueOf(o1.size()).compareTo(o2.size());
			}
		});

	}

	// devuelve true si el conjunto es dominante en el grafo
	private static boolean esDominante(Grafo g, HashSet<Integer> conjunto) {

		if (conjunto.size() == 0) {
			return false;
		}
		if (verticesVecinosAlConjunto(g, conjunto).containsAll(verticesDominablesPorElConjunto(g, conjunto)))
			return true;

		return false;
	}

	// devuelve el conjunto de vertices dentro del grafo y fuera del conjunto
	protected static HashSet<Integer> verticesDominablesPorElConjunto(Grafo g, HashSet<Integer> conjunto) {
		if (g.getCantidadDeVertices() == 0)
			return new HashSet<Integer>();
		if (!g.getIdsVertices().containsAll(conjunto))
			throw new IllegalArgumentException("El conjunto ingresado no es un subconjunto de los vertices del grafo");

		if (conjunto.size() == 0) {
			HashSet<Integer> verticesDominables = new HashSet<Integer>();
			verticesDominables.addAll(g.getIdsVertices());
			return verticesDominables;
		}
		ArrayList<Integer> verticesDominables = (ArrayList<Integer>) g.getIdsVertices();
		for (Integer id : g.getIdsVertices()) {// recorro los vertices de g
			if (conjunto.contains(id)) {// si el conjunto ingresado contiene un vertice
				verticesDominables.remove(id);// lo quito de los vertices dominables
			}
		}

		HashSet<Integer> copia = new HashSet<Integer>();
		copia.addAll(verticesDominables);
		return copia;
	}

	// devuelve los vertices vecinos al conjunto, dentro del grafo
	protected static HashSet<Integer> verticesVecinosAlConjunto(Grafo g, HashSet<Integer> conjunto) {

		if (conjunto.size() == 0)
			return new HashSet<Integer>();

		if (!g.getIdsVertices().containsAll(conjunto))
			throw new IllegalArgumentException("El conjunto ingresado no es un subconjunto de los vertices del grafo");

		HashSet<Integer> verticesVecinosAlConjunto = new HashSet<Integer>();
		for (Integer n : conjunto) {
			for (Vertice vecino : g.getVerticePorId(n).getVecinos()) {
				verticesVecinosAlConjunto.add(vecino.getId());
			}

		}
		return verticesVecinosAlConjunto;
	}

	// getter de variable auxiliar "actual", crea una copia para evitar aliasing
	private static HashSet<Integer> getActual() {
		@SuppressWarnings("unchecked")
		HashSet<Integer> copia = (HashSet<Integer>) actual.clone();
		return copia;
	}

	// conversor de List a HashSet
	private static HashSet<Integer> toHashSetInteger(List<Integer> conjunto) {
		HashSet<Integer> lista = new HashSet<Integer>();
		for (Integer n : conjunto) {
			lista.add(n);
		}
		return lista;
	}

}
