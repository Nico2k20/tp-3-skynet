package logica;

import static org.junit.Assert.*;


import org.junit.Test;

public class _GrafoTest {

	@Test
	public void testGrafoValido() {
		Grafo g = new Grafo(3);
		boolean gTiene3Vertices = g.getCantidadDeVertices() == 3;
		boolean gNoTieneAristas = g.getAristas().size() == 0;
		assertTrue(gTiene3Vertices && gNoTieneAristas);
	}

	@Test(expected = IllegalArgumentException.class)
	public void testGrafoInvalido() {
		@SuppressWarnings("unused")
		Grafo g = new Grafo(-1);
	}
	
	@Test
	//Debe devolver true ya que el grafo no tiene vecinos 
	public void testTieneVecinos1() {
		Grafo g = new Grafo(5);
		
		assertTrue(g.verticesAislados());
	}
	
	@Test
	//Debe devolver false ya que el grafo tiene vecinos
	public void testTieneVecinos2() {
		Grafo g = new Grafo(5);
		
		g.agregarArista(1, 0);
		
		assertFalse(g.verticesAislados());
	}
	
	

	@Test(expected = IllegalArgumentException.class)
	public void testAgregarAristaInvalidoPorVerticeNegativo() {
		Grafo g = new Grafo(3);
		g.agregarArista(-1, 0);
	}

	@Test(expected = IllegalArgumentException.class)
	public void testAgregarAristaInvalidoPorVerticeInexistente() {
		Grafo g = new Grafo(3);
		g.agregarArista(1, 3);
	}


	@Test
	public void existeAristaEntre() {
		Grafo g = new Grafo(3);
		g.agregarArista(1, 2);
		assertTrue(g.existeAristaEntre(1, 2));
	}

	public void testAgregarAristaValida() {
		Grafo g = new Grafo(3);
		g.agregarArista(0, 1);
		boolean vertice0esVecinoDe1 = g.getVertices().get(0).esVecinoDe(g.getVertices().get(1));
		boolean vertice1esVecinoDe0 = g.getVertices().get(1).esVecinoDe(g.getVertices().get(0));

		boolean laAristaTieneExtremo0 = g.getAristas().get(0).getExtremo1().compareTo(g.getVertices().get(0)) == 0;
		boolean laAristaTieneExtremo1 = g.getAristas().get(1).getExtremo1().compareTo(g.getVertices().get(1)) == 0;

		assertTrue(vertice0esVecinoDe1 && vertice1esVecinoDe0 && laAristaTieneExtremo0 && laAristaTieneExtremo1);
	}
	
	@Test
	public void testGrafoCompleto() {
		Grafo g = new Grafo(3);
		
		grafoK3(g);
		
		assertTrue(g.grafoCompleto());
	}
	
	@Test
	public void testGrafoCompletoK4() {
		Grafo g = new Grafo(4);
		
		grafoK4(g);
		
		assertTrue(g.grafoCompleto());
	}
	
	
	

	private void grafoK4(Grafo g) {
		g.agregarArista(0, 1);
		g.agregarArista(0, 2);
		g.agregarArista(0, 3);
		g.agregarArista(1, 2);
		g.agregarArista(1, 3);
		g.agregarArista(2, 3);
		
	}

	private void grafoK3(Grafo g) {

		g.agregarArista(0, 1);
		g.agregarArista(0, 2);
		g.agregarArista(1, 2);
		
	}

}
