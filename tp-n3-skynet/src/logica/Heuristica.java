package logica;

import java.util.ArrayList;
import java.util.Collections;

public class Heuristica {

	protected static ArrayList<ArrayList<Vertice>> algoritmoHeuristico(Grafo grafo) {
		ArrayList<Vertice> vertices = grafo.getCopiaVertices();

		if (vertices.size() == 0)
			throw new IllegalArgumentException("Cantidad de vertices 0");

		ArrayList<ArrayList<Vertice>> verticesDominantes = new ArrayList<ArrayList<Vertice>>();

		if (grafo.grafoCompleto()) {
			for (Vertice v : vertices) {
				ArrayList<Vertice> agregarVertice = new ArrayList<Vertice>();
				agregarVertice.add(v);
				verticesDominantes.add(agregarVertice);

			}
			return verticesDominantes;
		}

		if (buscarVerticeUnicoDominante(vertices)) {
			ArrayList<Vertice> verticeUnicoDom = new ArrayList<Vertice>();
			verticeUnicoDom.add(vertices.get(0));
			verticesDominantes.add(verticeUnicoDom);
			return verticesDominantes;
		}

		// Si solo tiene vertices aislados devuelve el conj de vertices
		if (grafo.verticesAislados()) {
			verticesDominantes.add(grafo.getCopiaVertices());
			return verticesDominantes;
		}

		for (Vertice v : vertices) {
			int diferencia = vertices.size();

			ArrayList<Vertice> verticesNoVecinos = conjVerticesNoVecinosConV(vertices, v);

			ArrayList<Vertice> dom = buscarConjDominante(v, diferencia, verticesNoVecinos, grafo);

			if (llegaAtodoVertice(vertices, dom))
				verticesDominantes.add(dom);
		}

		return verticesDominantes;
	}

	protected static boolean llegaAtodoVertice(ArrayList<Vertice> vertices, ArrayList<Vertice> dom) {

		boolean esta = false;
		for (Vertice vertice : vertices) {
			esta = false;

			for (Vertice vertice2 : dom) {
				if (vertice.equals(vertice2)) {
					esta = true;
					continue;
				}
				for (Vertice vertice3 : vertice2.getVecinos()) {
					if (vertice.equals(vertice3)) {
						esta = true;
						continue;
					}
				}

			}

			if (esta == false)
				return false;

		}

		return true;
	}

	private static ArrayList<Vertice> buscarConjDominante(Vertice vertice, int diferencia, ArrayList<Vertice> vertices,
			Grafo grafo) {

		ArrayList<Vertice> verticesDom = new ArrayList<Vertice>();
		verticesDom.add(vertice);
		boolean encontre = false;
		int sumatoriaGrados = vertice.getGrado() + 1;

		for (Vertice v : vertices) {
			if (v.getId() != vertice.getId()) {

				if (v.getGrado() + vertice.getGrado() == diferencia) {
					encontre = true;
					verticesDom.add(v);
				}

			}
		}

		if (!encontre) {
			for (Vertice vert : vertices) {
				if (vert.getId() != vertice.getId()) {

					if (diferencia - vert.getGrado() + sumatoriaGrados >= 0 && BFS.esConexo(grafo) == true) {
						sumatoriaGrados += vert.getGrado();
						verticesDom.add(vert);
					}

					else if (diferencia - vert.getGrado() + 1 + sumatoriaGrados > 0 && BFS.esConexo(grafo) == false) {
						sumatoriaGrados += vert.getGrado() + 1;
						verticesDom.add(vert);

					}

				}

			}

		}

		return verticesDom;
	}

// Devuelve el conj de vertices pasado como argumento ordenado de mayor a menor
// grado
	protected static ArrayList<Vertice> dameArrayOrdenado(ArrayList<Vertice> vertices) {

// ordena por grado
		Collections.sort(vertices, (p, q) -> {
			if (p.getGrado() > q.getGrado())
				return 1;
			else if ((p.getGrado() == q.getGrado()))
				return 0;
			else
				return -1;
		});

		Collections.reverse(vertices);

		return vertices;
	}

// Devuelve un conjunto con conjuntos de vertices no vecinos
	protected static ArrayList<ArrayList<Vertice>> conjVerticesNoVecinos(ArrayList<Vertice> vertices) {

		ArrayList<ArrayList<Vertice>> conjVerticesNoVecinos = new ArrayList<ArrayList<Vertice>>();

		for (Vertice v : vertices) {
			ArrayList<Vertice> verticesNoVecinos = conjVerticesNoVecinosConV(vertices, v);
			conjVerticesNoVecinos.add(verticesNoVecinos);

		}

		return conjVerticesNoVecinos;

	}

// devuelve el conj de vertices que no son vecinos con el vertice pasado como
// argumento
	protected static ArrayList<Vertice> conjVerticesNoVecinosConV(ArrayList<Vertice> vertices, Vertice vertice) {
		ArrayList<Vertice> verticesNoVecinos = new ArrayList<Vertice>();

		for (Vertice v : vertices) {

			if (!(vertice.esVecinoDe(v))) {
				int i = 0;
				boolean agregar = true;
				while (i < verticesNoVecinos.size()) {
					if (verticesNoVecinos.get(i).esVecinoDe(v))
						agregar = false;
					i++;

				}
				if (agregar)
					verticesNoVecinos.add(v);

			}

		}

		return verticesNoVecinos;
	}

	public static boolean buscarVerticeUnicoDominante(ArrayList<Vertice> vertices) {
// O(1)
// busca si hay un unico vertice dominante
// Grado n-1

		return vertices.get(0).getGrado() + 1 == vertices.size();

	}

}
