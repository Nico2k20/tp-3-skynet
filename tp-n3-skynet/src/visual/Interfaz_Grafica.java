package visual;

import java.awt.EventQueue;
import java.awt.Font;
import java.awt.GridLayout;
import java.awt.Point;

import java.util.ArrayList;

import javax.swing.JFrame;
import javax.swing.JOptionPane;

import javax.swing.JPanel;
import java.awt.BorderLayout;

import java.awt.Dimension;

import javax.swing.JRadioButton;
import javax.swing.JScrollPane;

import javax.swing.SwingUtilities;

import controlador.Controlador;

import javax.swing.ButtonGroup;
import javax.swing.JCheckBox;
import javax.swing.JButton;

import javax.swing.JTextArea;

public class Interfaz_Grafica {

	private JFrame frame;

	private final JPanel panel = new JPanel();
	private JPanel panelMapa, panelControlMatriz, panelMatrizDeAdya;
	private ButtonGroup grupoJradioButton;
	private JRadioButton AgregarMarcadores, cursor;
	private ArrayList<ArrayList<JCheckBox>> botones;
	private JTextArea txtrEstadistica;
	private JScrollPane scrollPane;
	private JButton generarJSON, levantarJSON, btnIngresarAristas, btnFormarGrafo, generarConjDOM, conjuntosDomRandom,
			btnFuerzaBruta;
	private JButton btnRemoverObj;
	private Dibujo dib;
	private Controlador controlador;
	private ArrayList<ArrayList<Integer>> indicesAristas;
	private Font font;
	private boolean aristaIngresada;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Interfaz_Grafica window = new Interfaz_Grafica(null, null);
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	// contructor del JFRAME
	public Interfaz_Grafica(Dibujo dib, Controlador controlador) {
		initialize(dib, controlador);
	}

	// inicializa todo los componentes
	private void initialize(Dibujo dib, Controlador controlador) {

		// INICIALIZA LA VENTANA
		frame = new JFrame();
		frame.setBounds(1600, 800, 1600, 900);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(null);
		panel.setBounds(12, 75, 789, 530);
		frame.setResizable(false);
		panel.setLayout(new BorderLayout(0, 0));
		aristaIngresada = false;

		// INICIALIZA EL PANEL DE DIBUJO
		this.dib = dib;
		panel.add(this.dib);

		// INICIALIZAR EL GRAFO
		this.controlador = controlador;

		// INICIALIZA EL ArrayList anidado de CHECKBOXES
		this.botones = new ArrayList<ArrayList<JCheckBox>>();
		indicesAristas = new ArrayList<ArrayList<Integer>>();

		// AGREGAMOS EL PANEL AL FRAME

		frame.getContentPane().add(panel);

		// INICIALIZAMOS Y AGREGAMOS EN EL FRAME EL PANEL MAPA
		this.panelMapa = new JPanel();
		this.panelMapa.setBounds(12, 0, 789, 33);
		frame.getContentPane().add(panelMapa);
		panelMapa.setLayout(null);

		// INICIALIZA EL PANEL MATRIZ DE ADYACENCIA
		this.panelMatrizDeAdya = new JPanel();
		this.panelMatrizDeAdya.setBounds(825, 70, 752, 781);
		frame.getContentPane().add(panelMatrizDeAdya);

		// INICIALIZA Y CONFIGURA LOS RADIO BUTTON
		this.grupoJradioButton = new ButtonGroup();
		this.AgregarMarcadores = new JRadioButton("Agregar Marcadores");
		AgregarMarcadores.setBounds(103, 0, 170, 23);
		this.grupoJradioButton.add(AgregarMarcadores);
		panelMapa.add(AgregarMarcadores);

		this.cursor = new JRadioButton("Cursor");
		this.cursor.setSelected(true);
		this.cursor.setBounds(19, 0, 80, 23);
		this.grupoJradioButton.add(cursor);
		panelMapa.add(cursor);

		// INICIALIZA EL PANEL QUE CONTROLA LA MATRIZ DE ADYACENCIA

		this.panelControlMatriz = new JPanel();
		panelControlMatriz.setBounds(825, 0, 397, 59);
		frame.getContentPane().add(panelControlMatriz);
		panelControlMatriz.setLayout(new BorderLayout(0, 0));

		// BOTON FORMAR GRAFO
		this.btnFormarGrafo = new JButton("Formar grafo");
		panelControlMatriz.add(btnFormarGrafo, BorderLayout.CENTER);

		// BOTON CONJ DOMINANTES
		this.generarConjDOM = new JButton("Conj Dominantes");
		generarConjDOM.setBounds(317, 44, 159, 26);
		generarConjDOM.addActionListener(e -> {
			if (this.dib.getSizePuntos() == 0) {
				JOptionPane.showMessageDialog(null, "Conjunto de vertices vacio");
				return;
			}
			if (!this.aristaIngresada) {
				JOptionPane.showMessageDialog(null, "Debe ingresar las aristas");
				return;
			}

			try {
				this.dib.dibujarConjuntoDominante(this.controlador.arrayListPostVerticesDom(), this.indicesAristas);
				setearEstadistica();
			} catch (Exception exc) {
				JOptionPane.showMessageDialog(null, "El grafo est� vacio");
			}
		});

		// INICIALIZA LOS BOTONES QUE CONTROLAN LA MATRIZ DE ADYACENCIA
		this.btnIngresarAristas = new JButton("Ingresar Aristas");
		panelControlMatriz.add(btnIngresarAristas, BorderLayout.WEST);

		btnIngresarAristas.addActionListener(e -> {

			aristaIngresada = true;
			mostrarSeleccionadorAristas();

		});

		frame.getContentPane().add(generarConjDOM);

		// BOTON CONJUNTO DOMINANTE RANDOM
		conjuntosDomRandom = new JButton("Conj Rand Dom");
		conjuntosDomRandom.setBounds(159, 44, 159, 26);
		frame.getContentPane().add(conjuntosDomRandom);
		conjuntosDomRandom.addActionListener(e -> {

			if (this.dib.getSizePuntos() == 0) {
				JOptionPane.showMessageDialog(null, "Conjunto de vertices vacio");
				return;
			}
			if (!this.aristaIngresada) {
				JOptionPane.showMessageDialog(null, "Debe ingresar las aristas");
				return;
			}
			try {
				this.dib.dibujarConjuntoDominante(this.controlador.arrayListPosVerticesDomRandom(), indicesAristas);

				setearEstadistica();
			} catch (Exception exc) {
				JOptionPane.showMessageDialog(null, "El grafo est� vacio");
			}

		});

		// BOTON REINICIAR
		btnRemoverObj = new JButton("Reiniciar");
		btnRemoverObj.setBounds(12, 44, 147, 26);
		frame.getContentPane().add(btnRemoverObj);
		btnRemoverObj.addActionListener(e -> {
			reiniciar();

		});

		// SE SETEA EL JTEXT AREA PARA LAS ESTADISTICAS
		scrollPane = new JScrollPane();
		scrollPane.setBounds(12, 616, 789, 235);
		frame.getContentPane().add(scrollPane);
		txtrEstadistica = new JTextArea();
		scrollPane.setViewportView(txtrEstadistica);
		txtrEstadistica.setEditable(false);
		txtrEstadistica.setText("Estadisticas");
		this.font = new Font("Tahoma", Font.BOLD, 15);
		txtrEstadistica.setFont(this.font);

		// BOTON GENERAR JSON
		generarJSON = new JButton("Generar JSON");
		generarJSON.setBounds(1221, 0, 181, 59);
		frame.getContentPane().add(generarJSON);

		// ACTION LISTENER DE BOTON FUERZA BRUTA
		btnFuerzaBruta = new JButton("Fuerza Bruta");
		btnFuerzaBruta.setBounds(475, 44, 198, 26);
		frame.getContentPane().add(btnFuerzaBruta);
		btnFuerzaBruta.addActionListener(e -> {

			if (this.dib.getSizePuntos() == 0) {
				JOptionPane.showMessageDialog(null, "Conjunto de vertices vacio");
				return;
			}
			if (!this.aristaIngresada) {
				JOptionPane.showMessageDialog(null, "Debe ingresar las aristas");
				return;
			}

			this.dib.dibujarConjuntoDominante(this.controlador.generarConjDomFuerzaBruta(), indicesAristas);
			setearEstadisticaFuerzaBruta();

		});

		// BOTON LEVANTAR JSON
		levantarJSON = new JButton("Levantar JSON");
		levantarJSON.setBounds(1396, 0, 181, 59);
		frame.getContentPane().add(levantarJSON);
		levantarJSON.addActionListener(e -> {

			String nombre = JOptionPane.showInputDialog("Ingrese el nombre del archivo guardado");

			try {
				this.aristaIngresada = true;

				this.controlador.reiniciarGrafos();

				this.controlador.levantarJSON(nombre);

				// dibuja los circulos
				this.dib.dibujarVertices(this.controlador.coordenadas());

				// Da los extremos de los vertices a graficar
				this.indicesAristas = new ArrayList<ArrayList<Integer>>();
				this.indicesAristas = this.controlador.darVecinos();

				// grafica las aristas
				this.dib.dibujarLineaSinResfrescar(this.indicesAristas);

			}

			catch (Exception ex) {
				// ex.printStackTrace();
				JOptionPane.showMessageDialog(null, "El archivo ingresado no existe");
			}

		});

		// GENERAR JSON
		generarJSON.addActionListener(e -> {

			String nombreArchivo = JOptionPane.showInputDialog("Por favor ingrese un nombre para su archivo JSON");

			this.controlador.crearJSON(nombreArchivo, this.dib.getPuntos());

		});

		btnFormarGrafo.addActionListener(e -> {
			this.controlador.reiniciarAristas();
			this.asignarArista();

		});

		frame.setVisible(true);

	}

	public void setearEstadistica() {

		this.txtrEstadistica.setText("Conjunto dominante minimo:\n" + this.controlador.StringconjDominanteMasChico()
				+ "\nConjuntos dominantes:\n" + this.controlador.verticesDominantes());

	}

	public void setearEstadisticaFuerzaBruta() {

		this.txtrEstadistica.setText(this.controlador.estadisticasFuerzaBruta());

	}

/////////////////////////METODOS MAPA///////////////////////////////////////////////////

	protected void agregarMarcador(Point puntoAlHacerCLick) {

		this.dib.dibujarVertice(puntoAlHacerCLick);

	}

	// Crea una matriz de n * n Jcheckbox,
	// N =cantidad de vertices
	private void mostrarSeleccionadorAristas() {
		this.botones = new ArrayList<ArrayList<JCheckBox>>();

		int tamanoMatrizAd = this.dib.getSizePuntos();

		if (tamanoMatrizAd == 0) {
			JOptionPane.showMessageDialog(null, "Conjunto de vertices vacio");
			return;
		}

		this.controlador.agregarNVertices(tamanoMatrizAd);

		this.panelMatrizDeAdya.removeAll();

		GridLayout layoutGrid = new GridLayout(tamanoMatrizAd, tamanoMatrizAd); // layout de n*n

		this.panelMatrizDeAdya.setLayout(layoutGrid); // seteamos el layout

		ArrayList<JCheckBox> botones; // arrayList de JCheckBox

		for (int i = 0; i < tamanoMatrizAd; i++) {

			botones = new ArrayList<JCheckBox>();
			for (int j = 0; j < tamanoMatrizAd; j++) {

				JCheckBox boton = new JCheckBox();
				boton.setText(i + ":" + j);

				if (i == j) // si el el checkbox es un bucle, se lo desactiva.
					boton.setEnabled(false);

				if (i < j) // si el el checkbox es un bucle, se lo desactiva.
					boton.setEnabled(false);

				boton.setPreferredSize(new Dimension(15, 15));

				botones.add(boton);
				// [boton1 boton2 boton3],[boton 11 boton 12 ,boton 13]
				this.panelMatrizDeAdya.add(boton);

			}

			this.botones.add(botones);

		}

		SwingUtilities.updateComponentTreeUI(this.panelMatrizDeAdya);

	}

	// Mira la matriz de JcheckBox se fija cual esta seleccionado y pide el
	// peso(agregar solo numeros)
	private void asignarArista() {
		this.indicesAristas = new ArrayList<ArrayList<Integer>>();

		for (int i = 0; i < this.botones.size(); i++) {

			for (int j = 0; j < this.botones.size(); j++) {
				ArrayList<Integer> indicesAristas = new ArrayList<Integer>();
				if (this.botones.get(i).get(j).isSelected()) {

					indicesAristas.add(i);
					indicesAristas.add(j);

					this.controlador.agregarArista(i, j);
					this.indicesAristas.add(indicesAristas);
				}

			}

		}

		this.dib.dibujarLineaSinResfrescar(this.indicesAristas);

	}

/////////////////LIMPIAR PANTALLA/////////////////////////////////////	

	private void reiniciar() {

		this.controlador.reiniciarGrafos();
		this.dib.vaciarPantalla();
		this.cursor.setSelected(true);
		this.botones = new ArrayList<ArrayList<JCheckBox>>();
		this.indicesAristas = new ArrayList<ArrayList<Integer>>();
		this.panelMatrizDeAdya.removeAll();
		this.txtrEstadistica.setText("Estadisticas");
		SwingUtilities.updateComponentTreeUI(this.panelMatrizDeAdya);
		this.aristaIngresada = false;

	}

/////////////////////Metodo JradioButton////////////////////////////////////	

	protected boolean estaApretadoMarcador() {
		return this.AgregarMarcadores.isSelected();
	}
}
