package visual;

import controlador.Controlador;

public class Main {

	public static void main(String[] args) {

		Dibujo dib = new Dibujo();

		Controlador controlador = new Controlador();
		Interfaz_Grafica gui = new Interfaz_Grafica(dib, controlador);
		EventosMouse mouse = new EventosMouse(dib, gui);
		dib.addMouseListener(mouse);

	}

}
