package visual;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Point;
import java.awt.event.MouseListener;
import java.awt.geom.Ellipse2D;
import java.util.ArrayList;
import javax.swing.JPanel;

public class Dibujo extends JPanel {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private ArrayList<Point> puntos;

	// private boolean primeraVez;

	public Dibujo() {

		inicializarPanel();
	}

	private void inicializarPanel() {

		this.puntos = new ArrayList<Point>();

		// primeraVez=true;
		this.setVisible(true);

	}

	@Override
	public void paint(Graphics g) {
		super.paint(g);

//		if(!primeraVez) {
//		
//			if(puntos.size()>0&&this.puntosConectados.size()>0) {
//				dibujarVertices(g);
//				dibujarArista(puntosConectados,g);
//				if(this.indicesVerticesDom.size()>0)
//					dibujarConjuntoDominante(g);
//			 }
//		}
	}

	public void addMouseListenerPanel(MouseListener e) {
		this.addMouseListener(e);
	}

	// Dado un punto dibuja un vertice y guarda el mismo para posterior uso
	public void dibujarVertice(Point punto) {
		this.puntos.add(punto);
		paint(this.getGraphics());
		dibujarVertices(this.getGraphics());

	}

	// Dibuja los vertices que estan guardados en puntos
	protected void dibujarVertices(Graphics g) {
		int indice = 0;

		Graphics2D g2d = (Graphics2D) g;

		while (indice < this.puntos.size()) {

			g2d.setColor(Color.RED);
			Ellipse2D circulo = new Ellipse2D.Float(this.puntos.get(indice).x - 10, this.puntos.get(indice).y - 10, 20,
					20);
			g2d.fill(circulo);
			g2d.setColor(Color.BLACK);
			g2d.drawString(indice + "", this.puntos.get(indice).x - 10, this.puntos.get(indice).y - 10);
			indice++;
		}

	}

	// Dibuja los vertices dominantes
	protected void dibujarConjuntoDominante(ArrayList<Integer> dots, ArrayList<ArrayList<Integer>> indicesAristas) {
		paint(this.getGraphics());
		dibujarVertices(this.getGraphics());
		dibujarLineaSinResfrescar(indicesAristas);

		for (Integer integer : dots) {
			dibujarVerticeDOM(integer, this.getGraphics());

		}

	}

	// Busca el vertice dom con el indice y lo dibuja en el plano
	private void dibujarVerticeDOM(int indice, Graphics g) {

		g.setColor(Color.GREEN);
		g.drawOval(puntos.get(indice).x - 10, puntos.get(indice).y - 10, 20, 20);
		g.fillOval(puntos.get(indice).x - 10, puntos.get(indice).y - 10, 20, 20);
	}

	// Metodo sobrecargado que toma un array de puntos y dibuja circulos en la
	// coordenadas dadas
	protected void dibujarVertices(ArrayList<Point> puntos) {
		paint(this.getGraphics());

		Graphics2D g2d = (Graphics2D) this.getGraphics();

		this.puntos = puntos;

		int indice = 0;

		while (indice < this.puntos.size()) {

			g2d.setColor(Color.RED);
			Ellipse2D circulo = new Ellipse2D.Float(this.puntos.get(indice).x - 10, this.puntos.get(indice).y - 10, 20,
					20);
			g2d.fill(circulo);
			g2d.setColor(Color.BLACK);
			g2d.drawString(indice + "", this.puntos.get(indice).x - 10, this.puntos.get(indice).y - 10);
			indice++;
		}

	}

	// Dibuja una arista dado los conj de indices (Los indice son los vertices a
	// representar)
//	protected void dibujarArista(ArrayList<ArrayList<Integer>> indicesAristas,Graphics g) {
//		
//		Graphics2D g2d = (Graphics2D) g;
//		
//		for (int i = 0; i < indicesAristas.size(); i++) {
//			Point punto1 = this.puntos.get(indicesAristas.get(i).get(0));
//			Point punto2 = this.puntos.get(indicesAristas.get(i).get(1));
//
//			g2d.setColor(Color.BLUE);
//			g2d.drawLine(punto1.x, punto1.y, punto2.x, punto2.y);
//		}
//		dibujarVertices(g2d);
//
//	}

	// Dibuja las aristas sin refrescar la pantalla. Esto hace que se superponga con
	// lo dibujado
	// anteriormente
	protected void dibujarLineaSinResfrescar(ArrayList<ArrayList<Integer>> indicesAristas) {
		paint(this.getGraphics());

		Graphics2D g2d = (Graphics2D) this.getGraphics();

		for (int i = 0; i < indicesAristas.size(); i++) {
			Point punto1 = this.puntos.get(indicesAristas.get(i).get(0));
			Point punto2 = this.puntos.get(indicesAristas.get(i).get(1));

			g2d.setColor(Color.BLUE);
			g2d.drawLine(punto1.x, punto1.y, punto2.x, punto2.y);
		}
		dibujarVertices(g2d);

	}

	// Vacia el panel
	protected void vaciarPantalla() {
		this.puntos = new ArrayList<Point>();

		paint(this.getGraphics());

	}

	// Da la cantidad de vertices disponibles
	protected int getSizePuntos() {
		return this.puntos.size();
	}

	// Devuelve el array de puntos
	protected ArrayList<Point> getPuntos() {

		return this.puntos;
	}

}
