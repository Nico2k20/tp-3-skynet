package visual;

import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;

public class EventosMouse implements MouseListener {
	Dibujo dib;
	Interfaz_Grafica gui;

	EventosMouse(Dibujo dib, Interfaz_Grafica gui) {
		this.dib = dib;
		this.gui = gui;
	}

	@Override
	public void mouseClicked(MouseEvent e) {

		if (this.gui.estaApretadoMarcador())
			this.dib.dibujarVertice(e.getPoint());

	}

	@Override
	public void mouseEntered(MouseEvent arg0) {
		// TODO Auto-generated method stub

	}

	@Override
	public void mouseExited(MouseEvent arg0) {
		// TODO Auto-generated method stub

	}

	@Override
	public void mousePressed(MouseEvent arg0) {
		// TODO Auto-generated method stub

	}

	@Override
	public void mouseReleased(MouseEvent arg0) {
		// TODO Auto-generated method stub

	}

}
