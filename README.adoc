= Conjunto Dominante Mínimo

== Introducción:


Un conjunto dominante en un grafo es un conjunto A de vértices tal que todo vértice que no
está en A tiene un vecino en A. El problema de encontrar un conjunto dominante de menor
tamaño posible se llama el problema de conjunto dominante mínimo, y su versión de
decisión es NP-completa. El trabajo práctico consiste en implementar un algoritmo goloso
para este problema.

== Objetivos:

Se debe implementar una aplicación que, a partir de los datos del grafo, utilice un algoritmo
goloso para encontrar un conjunto dominante de tamaño tan pequeño como pueda. La
aplicación debe informar el conjunto obtenido. Para el ingreso del grafo, se puede
implementar cualquiera de estas dos opciones:

 * Dar al usuario la opción de ir cargando el grafo manualmente, con botones para
agregar vértices y para agregar aristas. Cuando se agrega una arista, se debe pedir
al usuario los dos extremos de la arista (que deben ser vértices existentes).
 * Leer el grafo desde un archivo, con el formato que el grupo determine. Puede ser un
archivo de texto plano o un archivo JSON, a elección del grupo.


== Problemas Encontrados:
* Asignar aristas: Al formar repetidas veces las aristas, Json tomaba aristas no
seleccionadas. Solución: se reinicia las arista del grafo antes de asignar las mismas
al grafo.
* Inicialmente, se diseñó una clase llamada Algoritmos para contener a los métodos
utilizados para buscar un conjunto dominante mediante heurística golosa y fuerza
bruta, lo que causó que esta clase tuviera una cantidad densa de código. Solución:
se dividió esta clase en las clases Heurística y Fuerza Bruta, reduciendo la densidad
de código en cada una.
* Implementar Backtracking : el algoritmo de Fuerza Bruta para hallar un conjunto
dominante lleva implementado un backtracking que lo vuelve más eficiente, las
soluciones parciales que se construyen son las primeras soluciones encontradas por
rama descartando las soluciones restantes de la rama.



== Tipos Abstractos de Datos utilizados
- El programa se dividió en 4 paquetes:
* “Lógica”, encargado de la parte lógica
* “Visual”, encargado de la interfaz gráfica del plugin Windows Builder
* “Controlador”, encargado de llamar a la parte lógica a través de la parte gráfica
* “Datos”, encargado de generar un archivo json


== Paquete Lógica:
- TAD Arista
* int peso; debe ser mayor o igual a 0
* Vertice extremo1; debe ser distinto de null
* Vertice extremo2; debe ser distinto de null
* Arista(Vertice u, Vertice v, int peso);
constructor
* int compareTo(Arista a);
devuelve 0 si las aristas tienen los mismos extremos y peso
* boolean equals(Object obj);
devuelve 0 si las aristas tienen los mismos extremos y peso

- TAD BFS
* ArrayList<Integer> L;
* boolean[] marcados;
* boolean esConexo(Grafo g);
 devuelve true si el grafo g es conexo
* Set<Integer> alcanzables(Grafo g, int origen);
devuelve un set con los ids de los vértices que son alcanzables en el grafo g a partir del
vértice origen
* void inicializar(Grafo g, int origen);
 inicializa las variables L y la cantidad de elementos de la variable marcados para un nuevo
recorrido del grafo g
* void agregarVecinosPendientes(Grafo g, int i);
agrega los vecinos del vértice con id “i” a la lista de pendientes
* void actualizarPadresPorAristaModificada(Grafo g, int i, int j);
actualiza los padres de todos los vertices de un grafo al agregar o quitar una arista
* void actualizarPadresDeComponenteConexa(Grafo g, int id);
actualiza los padres de todos los vértices de la región de un grafo

- TAD Grafo
* List <Integer> raices; sus elementos deben ser ids de vértices que estén dentro del
 grafo, su tamaño debe ser igual a la cantidad de regiones del grafo y no debe
 contener elementos repetidos
* List <Vertice> vertices; no debe contener elementos repetidos
* ArrayList <Arista> aristas; no debe contener elementos repetidos
* Grafo (int n); constructor
* void agregarArista (int i, int j, int peso);
agrega una nueva arista al grafo entre los vertices con id “i” y “j” y con el peso ingresado.
* String listaVecinos ();
* boolean verticesAislados ();
* boolean grafoCompleto ();
* boolean existeAristaEntre (int i, int j);
 devuelve true si existe una arista entre los vértices con id “i” y “j”
* ArrayList<Arista> copiaAristas ();
* void agregarVertice (Vertice v);
agrega un nuevo objeto tipo Vertice a la lista de vértices del grafo
* void agregarNVertices (int n);
 agrega n objetos tipo Vertice al grafo
* boolean contieneAlVertice (Vertice v);
devuelve true si el grafo contiene al Vertice v
* Arista aristaDeMayorPeso();
devuelve la arista de mayor peso del grafo
* boolean equals(Object obj);
devuelve true si ambos grafos tienen el mismo conjunto de vértices y aristas
* String verticesDominantes();
* ArrayList<Integer> conjDomMinimoFuerzaBruta();
* String estadisticasFuerzaBruta();
* void levantarVerticesYaristas(int puntoA, int puntoB);

* void vaciarGrafo(); 

- TAD Vertice
* int id; debe ser mayor a 0
* int padre; debe ser mayor o igual a 0
* Set<Vertice> vecinos;
* Vertice (int id); Constructor
* void agregarVecino (Vertice v);
 agrega a la lista de vecinos al Vertice v
* void quitarVecino(Vertice v);
quita de la lista de vecinos al Vertice v
* boolean esVecinoDe(Vertice v);
devuelve true si es vecino del Vertice v
* boolean tieneVecinos();
* compareTo(Vertice otro);
devuelve 0 si ambos vértices tienen la misma id
* boolean equals(Object obj);
devuelve true si ambos vértices tienen la misma id
- TAD Heuristica
//El grafo no puede ser igual a null
* ArrayList<ArrayList<Vertice>> algoritmoHeuristico(Grafo grafo);
* boolean llegaAtodoVertice (ArrayList<Vertice> vertices,ArrayList<Vertice> dom);
* ArrayList<Vertice> buscarConjDominante (Vertice vertice, int diferencia,
* ArrayList<Vertice> vertices,Grafo grafo);
* ArrayList<Vertice> dameArrayOrdenado(ArrayList<Vertice> vertices);
* ArrayList<ArrayList<Vertice>> conjVerticesNoVecinos(ArrayList<Vertice> vertices);
* ArrayList<Vertice> conjVerticesNoVecinosConV(ArrayList<Vertice> vertices, Vertice
vertice);
* boolean buscarVerticeUnicoDominante(ArrayList<Vertice> vertices);

- TAD FuerzaBruta
* HashSet<Integer> actual; //debe ser distinto de null
* private static ArrayList<HashSet<Integer>> subConjuntos; //los hashset no deben
repetirse
* ArrayList<HashSet<Integer>> todosLosSubconjuntosDeVertices(Grafo g);
* void generarDesde(Grafo g, int vertice);
* HashSet<Integer> verticesDominablesPorElConjunto(Grafo g, HashSet<Integer>
conjunto);
* HashSet<Integer> verticesVecinosAlConjunto(Grafo g, HashSet<Integer> conjunto);
* ArrayList<HashSet<Integer>> conjuntosDominantes(Grafo g);
devuelve todos los posibles conjuntos dominantes de g
* ArrayList<HashSet<Integer>> conjuntosDominantesMinimos(Grafo g);
* HashSet<Integer> conjuntoDominanteMinimoAlAzar(Grafo g);
* ArrayList<Integer> toArrayListInteger(HashSet<Integer> conjunto);

== Paquete Controlador :
- TAD Controlador :

* Grafo grafo;
* Json json;
* Controlador();
constructor
* void agregarNVertices(int cantVertices);
Setea el grafo con N vértices.
* agregarArista(int puntoA, int puntoB);
 Agrega una arista entre los vértices a y b.
* String verticesOrdenados();
* String verticesOrdenadosNoVecinos();

* String listaVecinos();
* ArrayList<Integer> generarConjDomFuerzaBruta();
* String estadisticasFuerzaBruta();
* String verticesDominantes();
* String StringconjDominanteMasChico();
* ArrayList<Integer> arrayListPostVerticesDom();
* ArrayList<Integer> arrayListPosVerticesDomRandom();
* void crearJSON(String nombre,ArrayList<Point> puntos);
* void agregarCoordenada(ArrayList<Point> puntos,Json json);
* void agregarAristaJSON(ArrayList<Arista> copiaAristas, int i);
* void levantarJSON(String nombre);
* ArrayList<Point> coordenadas();
* ArrayList<ArrayList<Integer>> darVecinos();
* reiniciarGrafos();
* inicializa todas las variables de instancia

== Paquete Datos:
- TAD Json:

* ArrayList<RepresentacionGrafo> aristas;
* private int cantidad_Vertices;
* private ArrayList<Point> puntos;
* Json();
* void crearJSON(String str);
* Json leerArchivoJSON(String archivo);
* String generarJSON();

== Paquete visual:

- TAD Dibujo:

* long serialVersionUID = 1L;
* ArrayList<Point> puntos;
* ArrayList<ArrayList<Integer>> puntosConectados;
* ArrayList<Integer> indicesVerticesDom;
* Dibujo(); Constructor
* void inicializarPanel();
* void paint(Graphics g);
* void addMouseListenerPanel(MouseListener e);
* void dibujarVertice(Point punto);
* void dibujarVertices(Graphics g);
* void dibujarConjuntoDominante(Graphics g);
* void dibujarConjuntoDominante(ArrayList<Integer> dots,
* ArrayList<ArrayList<Integer>> indicesAristas);
* void dibujarVerticeDOM(int indice, Graphics g);
* void dibujarVertices(ArrayList<Point> puntos);
* void dibujarArista(ArrayList<ArrayList<Integer>> indicesAristas,Graphics g);
* void dibujarLineaSinResfrescar(ArrayList<ArrayList<Integer>> indicesAristas);
* void vaciarPantalla();
- TAD EventosMouse:
* Interfaz_Grafica gui;
* Dibujo dib;
* EventosMouse(Interfaz_Grafica gui, Mapa mapa); Constructor
* void mouseClicked(MouseEvent e);
* void mouseEntered(MouseEvent arg0);
- TAD Interfaz_Grafica:

* JPanel panel;
* JPanel panelMapa, panelControlMatriz, panelMatrizDeAdya;
* ButtonGroup grupoJradioButton;
* JRadioButton AgregarMarcadores, cursor;
* ArrayList<ArrayList<JCheckBox>> botones;
* JTextArea txtrEstadistica;
* JScrollPane scrollPane;
* JButton generarJSON, levantarJSON, btnIngresarAristas, btnFormarGrafo,
 generarConjDOM, conjuntosDomRandom, btnFuerzaBruta;
* JButton btnRemoverObj;

* Dibujo dib;
* Controlador controlador;
* ArrayList<ArrayList<Integer>> indicesAristas;
* Font font;
* void agregarMarcador(Point puntoAlHacerClick);
Agrega un marcador dado un punto en el mapa.
* void mostrarSeleccionadorAristas();
Crea n*n JCheckBox a modo de matriz de adyacencia
* void asignarArista();
Asigna el peso(ingresado por el usuario) a las aristas.
* removerObjMapa();
Remueve todos los objetos del mapa
